import sys
sys.path.insert(0, '../../../')

import os
import glob
import json

import hyperspy.api as hs
import cv2

import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

import cupy as cp

from matplotlib_scalebar.scalebar import ScaleBar
from mpl_toolkits.axes_grid1 import make_axes_locatable

from temnn.analysis.analysis import F1_score

from scipy.ndimage.filters import maximum_filter, minimum_filter
from scipy.ndimage.measurements import center_of_mass, label
from cupyx.scipy import ndimage as cp_ndi
from cupyx.scipy.signal.signaltools import wiener
from cupyx.scipy.fft import fft2, fftshift, ifft

from tensorflow.keras import mixed_precision

class Simulated_Data:
    def __init__(self, npzfile, paramsfile, temparamsfile):
        """
        Initialises class with a path to a .npz containing the
        image and label, a .json containing data parameters,
        and a .json with ctf parameters
        """
        self.npzfile = npzfile
        self.paramsfile = paramsfile
        self.temparamsfile = temparamsfile
        
        self.is_infered = False

    def load(self):
        """
        Loads the simulated image and label as a numpy array
        """
        # Load data parameters
        with open(os.path.join(self.paramsfile)) as json_file1:
            par = json.load(json_file1)
        imgdim = tuple(par['image_size']) # spatial dimensions of input/output
        if par.get('multifocus', None):
            chan_in = par['multifocus'][0]
        else:
            chan_in = 1 # depth of input data
        chan_out = par['num_classes'] # number of predicted class labels
        self.normalizedistance = par['normalizedistance'] 
        
        # Load data
        d = np.load(self.npzfile)
        #if chan_in == 1:
        #    self.image = d['image'][0,:imgdim[0],:imgdim[1],0].astype(np.float32)
        #else:
        self.image = d['image'][0,:imgdim[0],:imgdim[1],:]
        #if chan_out == 1:
        #    self.label = d['label'][0,:imgdim[0],:imgdim[1],0].astype(np.int8)
        #else:
        self.label = d['label'][0,:imgdim[0],:imgdim[1],:]
        
        # Load tem parameters
        with open(self.temparamsfile) as json_file2:
            p = json.load(json_file2)
        self.sampling = p['sampling']
        
        json_file1.close()
        json_file2.close()

    def cp_local_standardise(self):
        """
        Performs a local standardisation of the image.
        
        sigma_px: float - Gaussian standard deviation in pixels
        
        no return, alters image
        """
        sigma = self.normalizedistance/self.sampling
        self.image = cp.asarray(self.image)
        channels = self.image.shape[-1]
        if channels > 1:
            B=cp.zeros_like(self.image[:,:,0])
            S=cp.zeros_like(self.image[:,:,0])
            for i in range(channels):
                B += cp_ndi.gaussian_filter(self.image[:,:,i],
                                            (sigma,sigma),
                                            mode='reflect')
            for i in range(channels):
                self.image[:,:,i] = self.image[:,:,i] - B/channels
            
            for i in range(channels):
                S += cp.sqrt(cp_ndi.gaussian_filter(self.image[:,:,i]**2,
                                                    (sigma,sigma),
                                                    mode='reflect'))
            for i in range(channels):
                self.image[:,:,i] = self.image[:,:,i] / (S/channels)
        else:
            self.image[:,:,0] = ((self.image[:,:,0] - cp.min(self.image[:,:,0])) / \
                    (cp.max(self.image[:,:,0]) - cp.min(self.image[:,:,0])))
            self.image[:,:,0] = self.image[:,:,0] \
                                 - cp_ndi.gaussian_filter(self.image[:,:,0],
                                         (sigma,sigma), mode='reflect')
            self.image[:,:,0] = self.image[:,:,0] \
                                 / cp.sqrt(cp_ndi.gaussian_filter(self.image[:,:,0]**2,
                                     (sigma,sigma),mode='reflect'))
        self.image = self.image.get()

        return self.image
    
    def infer(self, model):
        """
        Takes a neural networks and runs it's
        prediction on the image

        model - tensorflow neural network model

        returns:
        inference - numpy array
        """
        policy = mixed_precision.Policy('mixed_float16')
        mixed_precision.set_global_policy(policy)

        image = self.image.copy()
        image.shape = (1,) + image.shape

        self.inference = model.predict(image)
        self.is_infered = True

        return self.inference

    def get_F1_score(self):
        """
        Returns the F1 score
        """
        label = self.label.copy()
        label.shape = (1,) + label.shape
        if self.is_infered:
            return F1_score(label, self.inference)
        else:
            print("Please call self.segment_nanoparticle(model) first!")
    
    def plot(self):
        if self.is_infered:
            channels = self.image.shape[-1]
            out_channels = self.label.shape[-1]
            out_channels2 = self.inference.shape[-1]
            width =100.0
            
            nplots = channels+out_channels+out_channels2
            fig = plt.figure(figsize=(width,width/nplots))
            for i in range(channels):
                ax = fig.add_subplot(1, nplots, i+1)
            
                im = ax.imshow(self.image[:,:,i].T,
                        origin='lower', cmap='gray')
                ax.add_artist(ScaleBar(self.sampling*0.1,
                                       units='nm',
                                       length_fraction=0.3,
                                       color='k',
                                       frameon=True,
                                       location='lower right'))
                divider = make_axes_locatable(ax)
                cax1 = divider.append_axes("right", size="2%", pad=0.05)
                cbar = plt.colorbar(im, cax = cax1)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
            for j in range(out_channels):
                ax = fig.add_subplot(1, nplots, channels+j+1)
            
                im = ax.imshow(self.label[:,:,j].T,
                        origin='lower', cmap='gray')
                divider = make_axes_locatable(ax)
                cax2 = divider.append_axes("right", size="2%", pad=0.05)
                cbar = plt.colorbar(im, cax = cax2)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
            for k in range(out_channels2):
                ax = fig.add_subplot(1, nplots, channels+out_channels+k+1)
            
                im = ax.imshow(self.inference[0,:,:,k].T,
                        origin='lower', cmap='gray')
                divider = make_axes_locatable(ax)
                cax2 = divider.append_axes("right", size="2%", pad=0.05)
                cbar = plt.colorbar(im, cax = cax2)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
            plt.tight_layout()
            plt.show()
        else:
            channels = self.image.shape[-1]
            out_channels = self.label.shape[-1]
            width = 100.0
            
            nplots = channels+out_channels
            fig = plt.figure(figsize=(width,width/nplots))
            for i in range(channels):
                ax = fig.add_subplot(1, nplots, i+1)
            
                im = ax.imshow(self.image[:,:,i].T,
                        origin='lower', cmap='gray')
                ax.add_artist(ScaleBar(self.sampling*0.1,
                                       units='nm',
                                       length_fraction=0.3,
                                       color='k',
                                       frameon=True,
                                       location='lower right'))
                divider = make_axes_locatable(ax)
                cax1 = divider.append_axes("right", size="2%", pad=0.05)
                cbar = plt.colorbar(im, cax = cax1)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
            for j in range(out_channels):
                ax = fig.add_subplot(1, nplots, channels+j+1)
            
                im = ax.imshow(self.label[:,:,j].T,
                        origin='lower', cmap='gray')
                divider = make_axes_locatable(ax)
                cax2 = divider.append_axes("right", size="2%", pad=0.05)
                cbar = plt.colorbar(im, cax = cax2)
                cbar.ax.tick_params(labelsize=16)
                ax.set_xticks([])
                ax.set_yticks([])
        plt.tight_layout()
        return fig
