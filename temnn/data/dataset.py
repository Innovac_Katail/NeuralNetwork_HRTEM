import numpy as np
import matplotlib.pyplot as plt
import ase
import scipy

from ase import io
from ase.data import covalent_radii, vdw_radii
from temnn.data import mods
from temnn.data.labels import create_label
from abtem.waves import Waves
from abtem.noise import poisson_noise
from abtem.mtf import MTF
from skimage.filters import gaussian
from scipy.spatial.distance import cdist

def to_tensors(batch):
    images = np.concatenate([b.image for b in batch],axis=0)
    labels = np.concatenate([b.label for b in batch],axis=0)
    return images, labels
    
def mtf_func(q, c1, c2, c3):
    """ 
    Simple MTF scaled so q=q/2qn where qn is the Nyqvist freq.
    
    This is equivalent to q*s where s is the sampling.
    """
    return (1 - c1) / ( 1 + (q / c2)**c3 ) + c1

class DataEntry(object):
    """
    A Dataentry containing at least a wavefunction
    """
    def __init__(self,wave,points,model=None,image=None,label=None):        
        self.wave_file = wave     # abTEM wave object
        self.points_file = points # tuple of numpy arrays of coordinates
        self.model_file = model   # ase atoms object
        self.image_file = image   # numpy array of image
        self.label_file = label   # numpy array of labels
        self.reset()
    
    def load(self):
        # Load abTEM wave objects
        self._wave = Waves.read(self.wave_file)
        
        # Load points
        npzfile = np.load(self.points_file,allow_pickle=True)
        try:
            self._positions = npzfile['positions'] # Atomic positions
        except KeyError:
            self._positions = npzfile['sites']     # Atomic columns
        try:
            self._sites = npzfile['sites']         # Atomic columns
        except KeyError:
            self._sites = npzfile['positions']
        try:
            self._classes = npzfile['classes']     # Classes
        except KeyError:
            self._classes = None
        try:
            self._heights = npzfile['heights']     # Atomic column heights
        except KeyError:
            self._heights = None
        
        # Load ase atoms object
        if self.model_file is not None:
            self._model = io.read(self.model_file)
        
        # If possible load image and label
        if self.image_file is not None:
            self._image = np.load(self.image_file)
            if len(self._image.shape)==3:
                self._image = self._image.reshape((1,)+self._image.shape)      
        if self.label_file is not None:
            self._label = np.load(self.label_file)
            if len(self._label.shape)==3:
                self._label = self._label.reshape((1,)+self._label.shape)
    
    def create_image(self, ctf, sampling, blur, dose, MTF_param=None, concatenate=False):
        image_wave = self._wave.apply_ctf(ctf)
        # Intensity of wavefunction at image plane
        measurement_raw = image_wave.intensity() 
        # Remove 0 dimension of 3D array (*! be careful with frozen phonon !*)
        # Interpolate requires 2D array
        measurement_blur = measurement_raw.squeeze()
        # Add Gaussian blur
        measurement_blur = measurement_blur.gaussian_filter([blur,blur])
        # Resample to match detector resolution (experimental image resolution)
        measurement = measurement_blur.interpolate(sampling)
        # Add Poisson noise
        measurement_noise = poisson_noise(measurement,dose)
        # Apply MTF
        img_array = measurement_noise.array
        measurement_MTF = measurement_noise.copy()
        #s = self._wave.sampling
        s = (sampling,sampling)
        kx = np.fft.fftfreq(img_array.shape[0], d=s[0])*s[0]
        ky = np.fft.fftfreq(img_array.shape[1], d=s[1])*s[1]
        Kx, Ky, = np.meshgrid(kx, ky, indexing='ij')
        K = np.sqrt(Kx**2 + Ky**2)
        new_img_array = np.fft.ifft2(np.fft.fft2(img_array) * mtf_func(K,**MTF_param))
        measurement_MTF.array[:] = new_img_array.real
        
        image = measurement_MTF.array
        image = image.reshape((1,)+image.shape+(1,)).astype(np.float32)
        if concatenate:
            self._image = np.concatenate((self._image,image),axis=3)
        else:
            self._image = image
    
    def create_label(self,sampling,label,num_classes,null_class=False,width=None,shape=None):
        ## Set shape
        if shape is None:
            shape=self._image.shape[1:-1]
        
        ## Create labels 
        if label == 'Mask': # Semantic segmentation
            # To create multi-class segmenation (ex. nanoparticle and substrate) I will pass an object
            # of arrays containing positions of each object to be segmented.
            if isinstance(self._positions,object) and len(self._positions) == 2:
                positions = [x/sampling for x in self._positions]
                self._label=create_label(positions,
                                         shape=shape,
                                         label=label,
                                         num_classes=num_classes)
            else: # If a single array of positions is given then it will do single-class segmentation
                self._label=create_label(self._positions/sampling,
                                         shape=shape,
                                         label=label,
                                         num_classes=num_classes) 
        elif label == 'Blob': # Gaussian blob object localisation of nanoparticles
            # Fix Gaussian blob at centroid of points
            points = self._positions[:,:2]/sampling
            x = [p[0] for p in points]
            y = [p[1] for p in points]
            centroid = (sum(x) / len(x), sum(y) / len(y))
            
            # Gaussian width
            distances = [ np.abs(np.sqrt((p[0]-centroid[0])**2 + (p[1]-centroid[1])**2)) for p in points]
            w = np.ceil(max(distances)).astype(int) / 2
            
            self._label=create_label(self._positions/sampling,
                                     shape=shape,
                                     width=w,
                                     label=label)  
        elif label == 'Exitwave':
            wave = self._wave.array[0]
            oldshape = wave.shape
            newshape = self._image.shape[1:3]
            zoom = (newshape[0] / oldshape[0], newshape[1] / oldshape[1])
            real = scipy.ndimage.zoom(np.real(wave), zoom)
            imag = scipy.ndimage.zoom(np.imag(wave), zoom)
            self._label = np.empty(newshape + (2,))
            self._label[:,:,0] = real - 1
            self._label[:,:,1] = imag
        
        else: # Atomic scale segmentation of nanoparticles
            if not num_classes:   # Explicit label meaning no classes.
                self._label=create_label(self._positions[:,:2]/sampling,
                                         shape=shape,
                                         width=width,
                                         label=label)
            else:
                self._label=create_label(self._sites[:,:2]/sampling,
                                         shape=shape,
                                         width=width,
                                         label=label,
                                         classes=self._classes,
                                         null_class=null_class,
                                         num_classes=num_classes)
                
        self._label=self._label.reshape((1,)+self._label.shape)
    
    def pad(self,size):
        self._image=np.pad(self._image,((0,0),(0,size[0]-self._image.shape[1]),
                            (0,size[1]-self._image.shape[2]),(0,0)),'constant', constant_values=0)
        self._label=np.pad(self._label,((0,0),(0,size[0]-self._label.shape[1]),
                            (0,size[1]-self._label.shape[2]),(0,0)),'constant', constant_values=0)

    def crop(self, xamount, yamount, sampling=None):
        """Crop the image.

        xamount, yamount: 
        The total amount to remove at edges.  Half is removed from
        each edge.  If odd, the extra pixel is removed from the high end.
        """
        x1 = xamount // 2
        x2 = xamount - x1
        y1 = yamount // 2
        y2 = yamount - y1
        if self._image is not None:
            self._image = self._image[:,x1:-x2,y1:-y2,:]
        if self._label is not None:
            self._label = self._label[:,x1:-x2,y1:-y2,:]
        if self._sites is not None:
            self._sites = self._sites - np.array([xamount/sampling, yamount/sampling])
            
    def as_tensors(self,return_all=False):
        if return_all:
            return self._image,self._label,self._sites,self._classes
        else:
            return self._image,self._label
    
    def view(self,axes=None,show_positions=True):
        if axes is None:
            fig,axes = plt.subplots(1,2)
        
        if (self._positions is not None)&show_positions:
            axes[0].plot(self._positions[:,0],self._positions[:,1],'x')
            axes[1].plot(self._positions[:,0],self._positions[:,1],'x')
            
        axes[0].imshow(self._image[0,:,:,0].T,cmap='gray')
        axes[1].imshow(self._label[0,:,:,0].T,cmap='gray')
        
        plt.show()
    
    def reset(self):
        self._model = None
        self._wave = None
        self._label = None
        self._image = None
        self._sites = None
        self._classes = None
        
class DataSet(object):

    def __init__(self,entries=None):
        if entries is None:
            self._entries=[]
        else:
            self._entries=entries
        
        self._num_examples = len(self._entries)
        self._epochs_completed = 0
        self._index_in_epoch = 0
    
    @property
    def num_examples(self):
        return self._num_examples
    
    @property
    def epochs_completed(self):
        return self._epochs_completed
        
    @property
    def entries(self):
        return self._entries
        
    def reset(self):
        self._epochs_completed = 0
        self._index_in_epoch = 0

    def append(self,entry=None):
        if entry is None:
            entry = DataEntry()
        self._entries.append(entry)
        self._num_examples+=1

    def remove(self,index):
        del self._entries[index]
        self._num_examples-=1
    
    def entry(self,index):
        return self._entries[index]

    def split(self,number):
        part1 = DataSet(self._entries[:-number])
        part2 = DataSet(self._entries[-number:])
        return part1, part2
        
    def next_batch(self, batch_size, shuffle=True):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        # Shuffle for the first epoch
        
        if self._epochs_completed == 0 and start == 0:
            self._perm = np.arange(self._num_examples)
            if shuffle is not None:
                if shuffle is True:
                    np.random.shuffle(self._perm)
                else:
                    # shuffle is expected to be a RandomState object or similar
                    shuffle.shuffle(self._perm)
                    
        # Go to the next epoch
        if start + batch_size > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Get the rest examples in this epoch
            rest_num_examples = self._num_examples - start
            batch_rest_part = [self._entries[i] for i in self._perm][start:self._num_examples]
            
            if shuffle:
                np.random.shuffle(self._perm)
            
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size - rest_num_examples
            end = self._index_in_epoch
            
            batch_new_part = [self._entries[i] for i in self._perm][start:end]
            batch = batch_rest_part + batch_new_part
        else:
            self._index_in_epoch += batch_size
            end = self._index_in_epoch
            batch = [self._entries[i] for i in self._perm][start:end]

        return batch
