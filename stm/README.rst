These are a few files shamelessly taken from Jacob Madsen's
structural-template-matching project at
https://github.com/jacobjma/structural-template-matching

Since this project only needs a few helper files from said project, I
just copied them here.
