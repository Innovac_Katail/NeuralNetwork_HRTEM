import sys
#sys.path.insert(0, '../../')
#sys.path.insert(0, '../../../NeuralNetwork_HRTEM')
import os
import time
import shutil
import argparse
import numpy as np
from ase.io import write
from abtem.potentials import Potential
from abtem.waves import PlaneWave
from scipy.cluster.hierarchy import fcluster, linkage
from multiprocessing import Pool
from temnn.reproducible import make_reproducible

class SampleMaker:
    """Create samples of 2D materials.

    SampleMaker is a base class, a method called make_data that creates the atoms object
    must be defined.
    """
    beamenergy = 50e3   # eV
    use_cfg = False
    def run(self, first_number, last_number, dir_name, N, sampling):
        # Parameters
        L = sampling*N
        label_size = (N,N)

        for i in range(first_number, last_number): 
            atoms, positions, sites, classes = self.make_data(i)
            # Check that the datatypes are correct
            assert len(positions.shape) == 2
            assert positions.shape[1] == 2
            assert len(sites.shape) == 2
            assert sites.shape[1] == 2
            assert issubclass(positions.dtype.type, np.float)
            assert len(classes) == len(sites)
            assert issubclass(classes.dtype.type, np.integer)

            ## *** abTEM for TEM image ***
            # Determine number of points for each potential slice
            wave_xy = (N,N)
            newatoms=atoms[atoms.numbers!=6]
            atom_list =[atoms,newatoms]
            folder_list=['{0}/wave/wave_{1:04d}','{0}/wave_no_support/wave_{1:04d}']
            # Number of slices is set by atoms.cell[2,2] with a default thickenss of 0.5A
            for j,atom in enumerate(atom_list):
                potential = Potential(atom,
                              sampling=sampling,
                              gpts=wave_xy,
                              parametrization='kirkland',
                              projection='infinite')
                wave = PlaneWave(
                        energy=self.beamenergy #acceleration voltage in eV
                )
                
                exit_wave=wave.multislice(potential,pbar=False)
                exit_wave.write(folder_list[j].format(dir_name,i))
            np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i),
                        sites=sites,
                        positions=positions,
                        classes=classes
                    )
            if self.use_cfg:
                write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),atoms)
            else:
                write('{0}/model/model_{1:04d}.traj'.format(dir_name,i),atoms)
                # We want a compressed save - do it manually
            #np.savez_compressed('{0}/wave/wave_{1:04d}.npz'.format(dir_name,i),
            #                    wave.array, wave.energy, wave.sampling) 
            
            print('iteration', i, flush=True)

    def driver(args):
        self.run(**args)

    def make_data(self, index):
        """Make the atomic system and related data.

        The argument index is the number of the system being made - usually ignored.
        
        Returns (atoms, positions, classes) where
        
        atoms is the atomic system.

        positions is the (x,y) coordinates of the atomic columns

        classes are the classes of the atomic columns, or None if all the same class.
        """
        raise NotImplementedError("The method make_data() must be defined in a subclass")

class SampleMakerTrue2D(SampleMaker):
    """Makes samples of 2D materials where there are never multiple atoms in a column.

    An example of such materials could be graphene or h-BN, but not MoS2
    where there are two sulphur atoms in the same position.
    """
    numclasses = 1   # Just a single kind of atoms
    # classes = {42:0, 16:1}  Mapping from atomic numbers to classes
    
    def make_data(self, _unused):
        atoms = self.make_atoms()

        # Get positions directly from the atoms.
        pos = atoms.get_positions()
        atno = atoms.get_atomic_numbers()
        positions = pos[:,:2]
        if self.numclasses == 1:
            classes = np.zeros(len(atoms), int)
        else:
            classes = [self.classes[z] for z in atno]
            assert len(positions) == len(classes)
        sites=None
        return atoms, positions, sites, classes

class SampleMakerDistinctColumns(SampleMaker):
    """Makes samples of 2D materials where there can be multiple atoms in a column.

    An example of such materials could MoS2, where there are two sulphur
    atoms in the same position.
    
    Columns are classified according to which atoms are in a column if numclasses > 0
    """
    columndistance = 0.3   # Max horizontal distance between atoms in column, in Å.
    # numclasses = 4
    # classes = {(42,): 0, (16,16): 1, (16,):2}
    # defaultclass = 3     # This class is for any other combination of atomic numbers

    def make_data(self, _unused):
        atoms = self.make_atoms()

        # Create positions of the columns.
        positions = atoms.get_positions()[:,:2]
        z = atoms.get_atomic_numbers()
        clusters = fcluster(linkage(positions), self.columndistance, criterion='distance')
        unique = np.unique(clusters)
        # Now cluster is the column number each atom belongs to, and unique is a list of all columns
        sites = np.zeros((len(unique), 2))    # Positions of the colums
        classes = -np.ones(len(unique), int)  # classes of the columns.  Initialize to -1
        for i, u in enumerate(unique):
            sites[i] = np.mean(positions[clusters==u], axis=0)
            atnos = tuple(sorted(z[clusters==u]))
            try:
                c = self.classes[atnos]
            except KeyError:
                c = self.defaultclass
            classes[i] = c
        return atoms, positions, sites, classes
    
def main(driverfunction, scriptfile):
    """Main script driver.  

    Requires two arguments, the driver function and the name of the main script file.
    """
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', 'number')
    parser.add_argument("folder", help="The name of the folder (in ../simulation_data) where the output is placed.")
    parser.add_argument("number", type=int,
                        help="The desired number of training examples (incl. any that are already done).")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.set_defaults(test=False)
    parser.add_argument("--sampling", default=0.1, type=float,
                        help="Sampling of wave function in Å/pixel") # old default 0.05
    parser.add_argument("--arraysize", default=1024, type=int,
                        help="Size of wavefunction array")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    args = parser.parse_args()
    
    if args.numproc > args.number - args.start:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('../../', 'simulation_data', args.folder)

    print("Generating samples {} to {} in folder {} using {} process(es)".format(
        args.start, args.number, dir_name, numproc))

    seed = np.random.SeedSequence(args.seed)
    print("np.random.SeedSequence entropy:", seed.entropy)

    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'wave_no_support', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))

    # Keep a copy of this script for reference
    shutil.copy2(__file__, dir_name)
    shutil.copy2(scriptfile, dir_name)

    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce_waves.sh'),
                        parser.prog,
                        argdict,
                        positionalparams)
    
    before = time.time()
    
    if numproc == 1:
        # Running on a single core.
        driverfunction(args.start, args.number, dir_name, args.arraysize, args.sampling, seed)
    else:
        data = []
        ndata = args.number - args.start
        seeds = seed.spawn(numproc)
        for i in range(numproc):
            data.append((
                args.start + i * ndata // numproc,
                args.start + (i+1) * ndata // numproc,
                dir_name,
                args.arraysize,
                args.sampling,
                seeds[i],
                ))
        with Pool(numproc) as pool:
            pool.starmap(driverfunction, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))
