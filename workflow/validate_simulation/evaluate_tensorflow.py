import sys
sys.path.insert(0, '../../../')
import os
import glob
import json
import platform
import contextlib
import time
import argparse
import shutil

import numpy as np

import cupy as cp

#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # or any {'0', '1', '2'}
#os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import tensorflow as tf
import tensorflow_addons as tfa

from tensorflow import keras
from tensorflow.keras import mixed_precision
from tensorflow.keras import layers

from skimage.filters import gaussian
from scipy.ndimage.filters import gaussian_filter

from cupyx.scipy import ndimage as cp_ndi

from datetime import datetime
from tqdm import tqdm

#########################################################
#### tensorflow mixed precision for memory efficiency ###
#########################################################
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_global_policy(policy)

#########################################################
################## data generator #######################
#########################################################
class datagenerator(tf.keras.utils.Sequence):
    def __init__(self, 
            batch_size, 
            img_size,
            data_paths,
            params_paths,
            input_channels,
            output_channels,
            augment=True):
         
         self.batch_size = batch_size
         self.img_size = img_size
         self.data_paths = data_paths
         self.params_paths = params_paths
         self.input_channels = input_channels
         self.output_channels = output_channels
         self.augment = augment
         
         assert len(self.data_paths) == len(self.params_paths)
         self.n = len(self.data_paths)
         self.indexes = np.arange(self.n)

    def on_epoch_end(self):
        'updates indexes after each epoch'
        #np.random.shuffle(self.indexes)
    
    def __getitem__(self, index):
        i = self.indexes[index * self.batch_size]
        batch_data_paths = self.data_paths[i : i + self.batch_size]
        batch_params_paths = self.params_paths[i : i + self.batch_size]

        return self.__dataloader(self.img_size,
                batch_data_paths, batch_params_paths,
                self.input_channels, self.output_channels)
    
    def __len__(self):
        return self.n // self.batch_size

    #################### data loader ########################
    def __dataloader(self, 
            img_size,
            data_paths,
            param_paths,
            input_channels,
            output_channels):
        x = np.zeros((len(data_paths), img_size[0], img_size[1], input_channels), dtype="float32")
        y = np.zeros((len(data_paths), img_size[0], img_size[1], output_channels), dtype="uint8")
        for j, (data_path, param_path) in enumerate(zip(data_paths, param_paths)):
            with open(param_path) as json_file:
                p = json.load(json_file)
            sigma = 12.0/p['sampling']
            data = np.load(data_path)
            
            img_tmp = data['image'][:,:img_size[0],:img_size[1],:]
            lbl_tmp = data['label'][:,:img_size[0],:img_size[1],:]
            if self.augment:
                img_tmp = random_brightness(img_tmp, -0.1, 0.1)
                img_tmp = random_contrast(img_tmp, 0.9, 1.1)
                img_tmp = random_gamma(img_tmp, 0.9, 1.1)
                img_tmp, lbl_tmp = random_flip(img_tmp, lbl_tmp)
            
            # Does not currently support multichannel !!!
            img_tmp = cp_local_normalise(img_tmp[0,:,:,0], sigma, sigma)
            img_tmp.shape = (1,) + img_tmp.shape + (1,) 
            x[j] = img_tmp.astype(np.float32)
            y[j] = lbl_tmp.astype(np.uint8)

        return x, y

#########################################################
################## image processing #####################
#########################################################
def cp_local_normalise(image, sigma1, sigma2):
    image = cp.asarray(image)
    image = image - cp_ndi.gaussian_filter(image,
                                           (sigma1,sigma1),
                                           mode='reflect')
    image = image / cp.sqrt(cp_ndi.gaussian_filter(image**2,
                                                   (sigma2,sigma2),
                                                   mode='reflect'))   
    return image.get()

def random_flip(images, labels, rnd=np.random.rand):
    for i in range(len(images)):
        if rnd() < .5:
            images[i,:,:,:] = np.fliplr(images[i,:,:,:])
            labels[i,:,:,:] = np.fliplr(labels[i,:,:,:])
            
        if rnd() < .5:
            images[i,:,:,:] = np.flipud(images[i,:,:,:])
            labels[i,:,:,:] = np.flipud(labels[i,:,:,:])
            
    return images,labels

def random_brightness(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        images[i,:,:,0]=images[i,:,:,0]+rnd(low,high)
    return images

def random_contrast(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        mean=np.mean(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-mean)*rnd(low,high)+mean
    return images
    
def random_gamma(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        min=np.min(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-min)*rnd(low,high)+min
    return images

#########################################################
################## START OF MAIN CODE ###################
#########################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("modf",
            help="The path and name of the folder where the trained neural network is placed..")
    parser.add_argument("vdatf",
            help="The path and name of the folder where the validation data is placed..")
    parser.add_argument("outf",
            help="The name of the folder where the output is placed.")
    args = parser.parse_args()
    
    ### Create output folder ###
    outf = args.outf
    if not os.path.exists(outf):
        print('Creating ', outf)
        os.makedirs(outf)
    ### Keep a copy of this script for reference ###
    shutil.copy2(__file__, outf)
    
    #########################################################
    ################ determine number of gpus ###############
    ### if there are gpus availables they will be located and
    ### utilised
    #########################################################
    cudavar = 'cuda_visible_devices'
    if cudavar in os.environ:
        cudadev = os.environ[cudavar]
        n_gpu = len(cudadev.split(','))
        print(cudavar, '=', cudadev)
        print("found {} gpu devices".format(n_gpu))
    else:
        n_gpu = 1
    # print on which host this is running (useful for troubleshooting on clusters).
    print("{}: running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))
    ## set up multi gpu
    btch = n_gpu # set batch size to the number of gpu's
    if n_gpu > 1: # if multi-gpu parallelisation is supported
        strategy = tf.distribute.mirroredstrategy()
        strategy_scope = strategy.scope
        print("*** replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == n_gpu
    else: # otherwise run on a single gpu in serial
        strategy_scope = contextlib.nullcontext
    
    ## load metadata
    vdatf = args.vdatf #'../simulation_data/Au_fcc_mixed_24102021-test'
    with open(os.path.join(vdatf, 'parameters.json')) as json_file:
        par = json.load(json_file)
    imgdim = tuple(par['image_size']) # spatial dimensions of input/output
    imgs_e = par['images_per_epoch']
    imgeps = par['image_epochs']
    if par.get('multifocus', None):
        chan_in = par['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = par['num_classes'] # number of predicted class labels
    print('Input channels: ', chan_in)
    print('Output channels: ', chan_out)
    if chan_out > 1:
        loss = 'categorical_crossentropy'
    else:
        loss = 'binary_crossentropy'
    
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    vinput_data_dir = vdatf + '/images_labels'
    vdata_paths = sorted(
        [
            os.path.join(vinput_data_dir, fname)
            for fname in os.listdir(vinput_data_dir)
            if fname.endswith(".npz")
            ])
    vinput_params_dir = vdatf + '/tem_params'
    vparams_paths = sorted(
        [
            os.path.join(vinput_params_dir, fname)
            for fname in os.listdir(vinput_params_dir)
            ])
    print('validation data:', len(vdata_paths), ' samples')
    val_gen = datagenerator(
            btch, imgdim, vdata_paths, vparams_paths, chan_in, chan_out,
            augment=False
    )

    model_files = glob.glob(args.modf+'/checkpoint-*')
    for modelf in model_files:
        print(modelf)
        ## compile neural network
        # keras callback to measure epoch time
        with strategy_scope():
            keras.backend.clear_session()
            model = keras.models.load_model(modelf)
        model.compile(
                optimizer='adam',
                loss=loss,
                metrics=['accuracy',
                    tf.keras.metrics.Precision(),
                    tf.keras.metrics.Recall()]
                )
        
        score = model.evaluate(
                val_gen,
                verbose=1)
        
        score_file = open('{}/score.txt'.format(outf), 'at')
        print(f'{score[0]}, {score[1]}, {score[2]}, {score[3]}',
              file=score_file)
        score_file.close()
        
    ## compile neural network
    # keras callback to measure epoch time
    print(args.modf+'/model-0')
    with strategy_scope():
        keras.backend.clear_session()
        model = keras.models.load_model(args.modf+'/model-0')
    model.compile(
            optimizer='adam',
            loss=loss,
            metrics=['accuracy',
                tf.keras.metrics.Precision(),
                tf.keras.metrics.Recall()]
            )
    
    score = model.evaluate(
            val_gen,
            verbose=1)
    
    score_file = open('{}/score.txt'.format(outf), 'at')
    print(f'{score[0]}, {score[1]}, {score[2]}, {score[3]}',
          file=score_file)
    score_file.close()
