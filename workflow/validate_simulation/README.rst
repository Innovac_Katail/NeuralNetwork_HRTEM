Files
====

learningcurve_onthefly.py
  Calculates the precision and recall for the simulated
  training data set and simulated validation (test)
  dataset. Images are generated on the fly.

  * This uses a homemade precision and recall calculation based on peaks detected in the inference and prediction
learningcurve_precomputed.py
  Calculates the precision and recall for the simulated
  training data set and simulated validation (test)
  dataset. precomputed images are loaded in.

  * This uses a homemade precision and recall calculation based on peaks detected in the inference and prediction
validation_plot.py/validation_plot.ipynb
  Plots the precision and recall from the above scripts.
learningcurve.bsub
  Sends Python script to the HPC GPU.
learningcurve.dtucompute.sh
  Sends Python script to dtu compute GPU.
ValidateExamples.ipynb
  Computes the F1 Score for all examples in the validation
  set. This is presented in a histogram, as a measure of
  the overall performance of the network.

  * This uses keras implemented functions for the precision and recall based on a point-by-point calculation, and a homemade F1-score wrapper function around keras.
ValidateMetrics-netvnet.ipynb
  Plots the Metric (Loss,Accuracy,Precision,Reacl) values
  computed and saved per epoch during training. The values
  from each network are plotted together for comparison.
  
  * This uses keras implemented functions for the precision and recall based on a point-by-point calculation
