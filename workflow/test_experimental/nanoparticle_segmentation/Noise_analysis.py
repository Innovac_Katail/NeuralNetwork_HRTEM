import sys
sys.path.insert(0, '../../../')
import re
import os
import glob

import numpy as np
import matplotlib.pyplot as plt

import hyperspy.api as hs

import tensorflow.keras as keras

from matplotlib_scalebar.scalebar import ScaleBar

from temnn.analysis.ExperimentalAnalysis import Experimental_Data, Fourier_Transform

from codetiming import Timer
from tqdm import tqdm

from pathlib import Path

def to_hs(im_array,im_ref):
    '''
    Function to convert a numpy array (im_array) into a hyperspy object 
    and set scale equal to that of a previously defined hyperspy object (im_ref)
    
    Returns a calibrated hyperspy object im_cal
    '''
    im_cal=hs.signals.Signal2D(im_array)
    
    im_cal.axes_manager.signal_axes[0].scale=im_ref.axes_manager.signal_axes[0].scale
    im_cal.axes_manager.signal_axes[1].scale=im_ref.axes_manager.signal_axes[1].scale
  
    im_cal.axes_manager.signal_axes[0].units=im_ref.axes_manager.signal_axes[0].units
    im_cal.axes_manager.signal_axes[1].units=im_ref.axes_manager.signal_axes[1].units
    
    return im_cal

def compute_dose_rate(intensity,brightness,area,exposure):
    '''
    Function to compute dose given intensity integrated over an area
    Brightness calibration and exposure time required
    
    Return dose in units of electrons per angstrom^2 per second
    '''
    dose_rate=(intensity*brightness_cal)/(area*exposure)/100
    
    return dose_rate

file_pattern = re.compile(r'.*?(\d+).*?')
def get_order(file):
    match = file_pattern.match(Path(file).name)
    if not match:
        return math.inf
    return int(match.groups()[0])

folder='../../experimental_data/wibang/20201201_ETEM_MEMS6/ROI1'
doserates=sorted(glob.glob('{}/DR*'.format(folder)),key=get_order)

output_dir='noise_analysis_output'
# Check if an output folder exists first, if not then creates one (will only create one the first time for a new dataset)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

##########################
### Compute dose rates ###
##########################
dose_rate_list=[]
scale_list = []
for DR in doserates:
    doserate = DR.split('/')[-1]
    print(doserate)
    files=glob.glob('{}/{}/Hour_00/Minute_*/Second_*/*.dm4'.format(folder,doserate))
    if not os.path.exists('{}/stack_{}.hspy'.format(output_dir,doserate)):
        try:
            i=hs.load(files,lazy=True,stack=True)
        except ValueError:
            continue
        i.save('{}/stack_{}.hspy'.format(output_dir,doserate),overwrite=True)
    i=hs.load('{}/stack_{}.hspy'.format(output_dir,doserate),lazy=False)

    stack_metadata=i.original_metadata.stack_elements.as_dictionary()
    frame_index=0
    frame_metadata=stack_metadata['element{}'.format(frame_index)]
    i.axes_manager.navigation_axes[0].scale=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
    i.axes_manager.navigation_axes[0].units='s'
    i.axes_manager.navigation_axes[0].name='t'
    scale_list.append((i.axes_manager.signal_axes[0].scale)*10)

    x_cal,y_cal=i.axes_manager.signal_axes[0].scale,i.axes_manager.signal_axes[1].scale
    x_size,y_size=i.axes_manager.signal_axes[0].size,i.axes_manager.signal_axes[1].size
    x_nm,y_nm=x_cal*x_size,y_cal*y_size

    im=to_hs(i.data,i)
    brightness_cal=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageData']['Calibrations']['Brightness']['Scale']
    exposure=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
    rect=hs.roi.RectangularROI(left=0.5,right=1.5,top=y_nm-3, bottom=y_nm-2) 
    im.plot()
    roi=rect.interactive(im, color="blue")
    roi_data=roi.data

    area=roi.inav[0].data.shape[0]*roi.inav[0].data.shape[1]*(x_cal*y_cal)
    for n in tqdm(range(0,roi_data.shape[0])):
        dose_rate_list.append(compute_dose_rate(roi_data[n].sum(),brightness_cal,area,exposure))
    plt.close('all')

dose_rates=np.array(dose_rate_list)
doses=dose_rates*exposure

####################
### Load Network ###
####################
nnf = '../../MSDnet/MSDnet_06012022_newmtf/model-0'
mod = keras.models.load_model(nnf)

######################
### Segment Frames ###
######################
frame_tot= len(dose_rate_list)
tot_frame_cnt = 0
for DR in tqdm(doserates, desc='Doserate', position=0):
    doserate = DR.split('/')[-1]
    print(doserate)
    files=glob.glob('{}/{}/Hour_00/Minute_*/Second_*/*.dm4'.format(folder,doserate))
    for im_file in tqdm(files, desc='Frame', position=1, leave=False):
        # Initialise experimental data class
        exp_data = Experimental_Data(im_file)

        frame_dose_rate=dose_rates[tot_frame_cnt]
        frame_dose=doses[tot_frame_cnt]
        frame_cumulative_dose=np.sum(doses[0:tot_frame_cnt+1])
        frame_time=np.round(exposure*tot_frame_cnt,2)
        
        # Analyse image
        image = exp_data.load()
        image = exp_data.cp_local_standardise()
        scale = exp_data.get_trimmed_imagescale()
        inference = exp_data.segment_nanoparticle(mod)

        ## Plotting
        fig,ax=plt.subplots(1,3,figsize=(36,12))
        ax[0].imshow(image.T,origin='lower',cmap='gray',vmin=scale[0],vmax=scale[1])
        #ax[0].text(x=0.135,y=0.88,s=r'Time : {} s'.format(frame_time),
        #    color='r',fontsize=20,transform=plt.gcf().transFigure)
        #ax[0].text(x=0.135,y=0.86,s=r'Dose rate: {} e$^-$/$\AA^2$ s'.format(
        #    np.format_float_scientific(frame_dose_rate,2)),
        #    color='r',fontsize=20,transform=plt.gcf().transFigure)
        #ax[0].text(x=0.135,y=0.56,s=r'Frame size: {} nm $\times$ {} nm'.format(
        #    np.round(x_nm,2),np.round(y_nm,2)),
        #    fontsize=14,transform=plt.gcf().transFigure)
        #ax[0].text(x=0.135,y=0.54,s=r'Frame dose: {} e$^-$/$\AA^2$'.format(
        #    np.format_float_scientific(frame_dose,2)),
        #    color='k',fontsize=14,transform=plt.gcf().transFigure)
        #ax[0].text(x=0.135,y=0.52,s=r'Cumulative dose: {} e$^-$/$\AA^2$'.format(
        #    np.format_float_scientific(frame_cumulative_dose,2)),
        #    color='k',fontsize=14,transform=plt.gcf().transFigure)
        ax[0].add_artist(ScaleBar(x_cal,
                                    units='nm',
                                    length_fraction=0.2,
                                    color='k',
                                    frameon=True,
                                    location='lower left'))
        ax[0].set_xticks([])
        ax[0].set_yticks([])
        
        ax[1].imshow(inference[0,:,:,0].T,origin='lower',cmap='inferno')
        ax[1].set_xticks([])
        ax[1].set_yticks([])
        
        for n in np.arange(0,len(dose_rates[:tot_frame_cnt+1])):
            ax[2].scatter(n*exposure,dose_rates[n],color='b',marker='s',s=7)
        ax[2].set_xlim(0-5,frame_tot*exposure+5)
        ax[2].set_ylim(np.min(dose_rates),np.max(dose_rates))
        ax[2].set_xlabel('time (s)')
        ax[2].set_ylabel('dose rate (e$^-$/$\AA^2$ s)')

        plt.tight_layout()
        #plt.subplots_adjust(bottom=0.2)
        
        plt.savefig('{}/noise_analysis_{}.png'.format(output_dir,str(tot_frame_cnt).zfill(4)))
        plt.close()
        tot_frame_cnt += 1
