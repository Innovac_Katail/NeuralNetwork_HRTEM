import numpy as np
import sys
import os
import re

if len(sys.argv) != 2:
    print("Usage: python {} folder".format(sys.argv[0]))
    sys.exit(1)
    
dir = sys.argv[1]

imagedir = os.path.join(dir, 'images')
labeldir = os.path.join(dir, 'labels')
outdir = os.path.join(dir, 'images_labels')

for d in (dir, imagedir, labeldir):
    if not os.path.exists(d):
        raise RuntimeError("Folder {} not found.".format(d))

if os.path.exists(outdir):
    raise RuntimeError("Output folder {} is already there.  Bailing out!".format(outdir))
os.mkdir(outdir)

n = 0
regexp = re.compile(r'image_([_\d]+).npy', flags=re.ASCII)
for f in os.listdir(imagedir):
    m = regexp.fullmatch(f)
    if m:
        number = m.group(1)
        # Read image
        image = np.load(os.path.join(imagedir, f))
        # Read label
        label = np.load(os.path.join(labeldir, 'label_'+number+'.npy'))
        # Sometimes labels are 64 bits.
        if label.dtype == np.float64:
            label = label.astype(np.float32)
        np.savez_compressed(os.path.join(outdir, 'image_label_'+number+'.npz'),
                            image=image, label=label)
        n += 1
        if n % 100 == 0:
            print(n)
    else:
        print("Skipping unrecognized file", f)
print()
print("Combined 2 * {} files.".format(n))

        
