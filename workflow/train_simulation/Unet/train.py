import sys
sys.path.insert(0, '../../../')
import os
import glob
import json
import platform
import contextlib
import time
import argparse
import shutil

import numpy as np

import cupy as cp
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # or any {'0', '1', '2'}
#os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import tensorflow as tf
import tensorflow_addons as tfa

from tensorflow import keras
from tensorflow.keras import mixed_precision
from tensorflow.keras import layers

from skimage.filters import gaussian
from scipy.ndimage.filters import gaussian_filter

from cupyx.scipy import ndimage as cp_ndi

from datetime import datetime
from tqdm import tqdm

#########################################################
#### tensorflow mixed precision for memory efficiency ###
#########################################################
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_global_policy(policy)

#########################################################
########## construct net architecture ###################
#########################################################
def get_model(img_dim,
              num_channels,
              output_layer):
    inputs = keras.Input(shape=img_dim)
    x = inputs
    
    skip = []
    # Downsampling blocks
    for channels in [64, 128, 256]:
        # Entry block
        x = conv_block(x, channels)
        previous_block_activation = x # set aside residual
        # Residual block
        x = conv_block(x, channels)
        x = conv_block(x, channels)
        x = conv_block(x, channels)
        x = layers.concatenate([x, previous_block_activation])
        # Exit block
        x = conv_block(x, channels)
        skip.append(x)
        # Pool
        x = layers.MaxPooling2D(pool_size=2, padding='same')(x)
    
    # Bridge
    # Entry block
    x = conv_block(x, 512)
    previous_block_activation = x # set aside residual
    # Residual block
    x = conv_block(x, 512)
    x = conv_block(x, 512)
    x = conv_block(x, 512)
    x = layers.concatenate([x, previous_block_activation])
    # Exit block
    x = conv_block(x, 512)
    
    # Upsampling blocks
    for i,channels in enumerate([256, 128, 64]):
        # Upsample
        x = layers.UpSampling2D(2, interpolation='bilinear')(x)
        x = conv_block(x, channels, kernel_size=1)
        x = layers.concatenate([x,skip[-(i+1)]])
        # Entry block
        x = conv_block(x, channels)
        previous_block_activation = x # set aside residual
        # Residual block
        x = conv_block(x, channels)
        x = conv_block(x, channels)
        x = conv_block(x, channels)
        x = layers.concatenate([x, previous_block_activation])
        # Exit block
        x = conv_block(x, channels)

    if output_layer == 'segment':
        if num_channels > 1:
            final_act = "softmax"
        else:
            final_act = "sigmoid"
        # add a per-pixel classification layer
        outputs = layers.Conv2D(
                    filters=num_channels,
                    kernel_size=1,
                    activation=final_act,
                    padding="same",
                    dtype='float32')(x)
    elif output_layer == 'regress':
        final_act = 'relu'
        # add a per-pixel regressional layer
        outputs = layers.Conv2D(
                    filters=num_channels,
                    kernel_size=1,
                    activation=final_act,
                    padding="same",
                    dtype='float32')(x)
    # define the model
    model = keras.Model(inputs, outputs)
    return model

def conv_block(x, filters, kernel_size=9):
    x = layers.Conv2D(
            filters=filters,
            kernel_size=kernel_size,
            padding="same",
            )(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)
    #x = layers.Dropout(rate=0.5)(x)
    
    return x

#########################################################
################## data generator #######################
#########################################################
class datagenerator(tf.keras.utils.Sequence):
    def __init__(self, 
            batch_size, 
            img_size,
            data_paths,
            params_paths,
            input_channels,
            output_channels,
            normalisation_distance,
            augment=True):
         
         self.batch_size = batch_size
         self.img_size = img_size
         self.data_paths = data_paths
         self.params_paths = params_paths
         self.input_channels = input_channels
         self.output_channels = output_channels
         self.normalisation_distance = normalisation_distance
         self.augment = augment
         
         assert len(self.data_paths) == len(self.params_paths)
         self.n = len(self.data_paths)
         self.indexes = np.arange(self.n)

    def on_epoch_end(self):
        'updates indexes after each epoch'
        np.random.shuffle(self.indexes)
    
    def __getitem__(self, index):
        i = self.indexes[index * self.batch_size]
        batch_data_paths = self.data_paths[i : i + self.batch_size]
        batch_params_paths = self.params_paths[i : i + self.batch_size]

        return self.__dataloader(self.img_size,
                batch_data_paths, batch_params_paths,
                self.input_channels, self.output_channels,
                self.normalisation_distance)
    
    def __len__(self):
        return self.n // self.batch_size

    #################### data loader ########################
    def __dataloader(self, 
            img_size,
            data_paths,
            param_paths,
            input_channels,
            output_channels,
            normalisation_distance):
        x = np.zeros((len(data_paths), img_size[0], img_size[1], input_channels))
        y = np.zeros((len(data_paths), img_size[0], img_size[1], output_channels))
        for j, (data_path, param_path) in enumerate(zip(data_paths, param_paths)):
            with open(param_path) as json_file:
                p = json.load(json_file)
            sigma = normalisation_distance/p['sampling']
            data = np.load(data_path)
            
            img_tmp = data['image'][:,:img_size[0],:img_size[1],:]
            lbl_tmp = data['label'][:,:img_size[0],:img_size[1],:]
            if self.augment:
                img_tmp = random_brightness(img_tmp, -0.1, 0.1)
                img_tmp = random_contrast(img_tmp, 0.9, 1.1)
                img_tmp = random_gamma(img_tmp, 0.9, 1.1)
                img_tmp, lbl_tmp = random_flip(img_tmp, lbl_tmp)
            
            img_tmp = cp_local_normalise(img_tmp[0,:,:,:], sigma, sigma)
            img_tmp.shape = (1,) + img_tmp.shape 
            x[j] = img_tmp
            y[j] = lbl_tmp

        return x, y

#########################################################
################## image processing #####################
#########################################################
def cp_local_normalise(image, sigma1, sigma2):
    image = cp.asarray(image)
    channels = image.shape[-1]
    if channels > 1:
        B=cp.zeros_like(image[:,:,0])
        S=cp.zeros_like(image[:,:,0])
        for i in range(channels):
            B += cp_ndi.gaussian_filter(image[:,:,i],
                                        (sigma1,sigma1),
                                        mode='reflect')
        for i in range(channels):
            image[:,:,i] = image[:,:,i] - B/channels
        
        for i in range(channels):
            S += cp.sqrt(cp_ndi.gaussian_filter(image[:,:,i]**2,
                                                (sigma2,sigma2),
                                                mode='reflect'))
        for i in range(channels):
            image[:,:,i] = image[:,:,i] / (S/channels)
    else:
        image[:,:,0] = ((image[:,:,0] - cp.min(image[:,:,0])) / (cp.max(image[:,:,0]) - cp.min(image[:,:,0])))
        image[:,:,0] = image[:,:,0] - cp_ndi.gaussian_filter(image[:,:,0],
                                                             (sigma1,sigma1),
                                                             mode='reflect')
        image[:,:,0] = image[:,:,0] / cp.sqrt(cp_ndi.gaussian_filter(image[:,:,0]**2,
                                                                     (sigma2,sigma2),
                                                                      mode='reflect'))   
    return image.get()

def random_flip(images, labels, rnd=np.random.rand):
    for i in range(len(images)):
        if rnd() < .5:
            images[i,:,:,:] = np.fliplr(images[i,:,:,:])
            labels[i,:,:,:] = np.fliplr(labels[i,:,:,:])
            
        if rnd() < .5:
            images[i,:,:,:] = np.flipud(images[i,:,:,:])
            labels[i,:,:,:] = np.flipud(labels[i,:,:,:])
    return images,labels

def random_brightness(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        images[i,:,:,0]=images[i,:,:,0]+rnd(low,high)
    return images

def random_contrast(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        mean=np.mean(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-mean)*rnd(low,high)+mean
    return images
    
def random_gamma(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        min=np.min(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-min)*rnd(low,high)+min
    return images

#########################################################
################## START OF MAIN CODE ###################
#########################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("epochs", default=10, type=int,
            help="Number of training epochs.")
    parser.add_argument("datf",
            help="The path and name of the folder where the training data is placed.")
    parser.add_argument("vdatf",
            help="The path and name of the folder where the validation data is placed..")
    parser.add_argument("outf",
            help="The name of the folder where the output is placed.")
    parser.add_argument("--restart", default=None, type=int, help="Restart at epoch N.")
    args = parser.parse_args()
    
    ### Create output folder ###
    if not args.outf.endswith('/'):
        outf = args.outf + '/'
    else:
        outf = args.outf
    if not os.path.exists(outf):
        print('Creating ', outf)
        os.makedirs(outf)
    ### Keep a copy of this script for reference ###
    shutil.copy2(__file__, outf)
    
    #########################################################
    ################ determine number of gpus ###############
    ### if there are gpus availables they will be located and
    ### utilised
    #########################################################
    cudavar = 'cuda_visible_devices'
    if cudavar in os.environ:
        cudadev = os.environ[cudavar]
        n_gpu = len(cudadev.split(','))
        print(cudavar, '=', cudadev)
        print("found {} gpu devices".format(n_gpu))
    else:
        n_gpu = 1
    # print on which host this is running (useful for troubleshooting on clusters).
    print("{}: running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))
    ## set up multi gpu
    btch = n_gpu # set batch size to the number of gpu's
    if n_gpu > 1: # if multi-gpu parallelisation is supported
        strategy = tf.distribute.mirroredstrategy()
        strategy_scope = strategy.scope
        print("*** replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == n_gpu
    else: # otherwise run on a single gpu in serial
        strategy_scope = contextlib.nullcontext
    
    ## load metadata
    datf = args.datf 
    vdatf = args.vdatf
    with open(os.path.join(datf, 'parameters.json')) as json_file:
        par = json.load(json_file)
    imgdim = tuple(par['image_size']) # spatial dimensions of input/output
    if par.get('multifocus', None):
        chan_in = par['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = par['num_classes'] # number of predicted class labels
    print('Input channels: ', chan_in)
    print('Output channels: ', chan_out)

    ## compile neural network
    label_type = par['label']
    if label_type == 'Mask' or label_type == 'Disk':
        if chan_out > 1:
            loss = 'categorical_crossentropy'
        else:
            loss = 'binary_crossentropy'
        output_layer = 'segment' 
        metrics=['accuracy',
            tf.keras.metrics.Precision(),
            tf.keras.metrics.Recall()]
    else: # (eg. 'Exitwave')
        loss = 'mse'
        output_layer = 'regress'
        metrics=['rmse']
    
    with strategy_scope():
        keras.backend.clear_session()
        if args.restart is not None:
            model = keras.models.load_model(
                        outf+'checkpoint-{:04d}'.format(args.restart))
            initial_epoch = args.restart
        else:
            model = get_model((None,None,chan_in),
                              chan_out,
                              output_layer)
            initial_epoch = 0
    model.compile(
           optimizer='adam',
           loss=loss,
           metrics=metrics
           )
    model.summary()
    
    #########################################################
    ################### prepare logfile #####################
    ### in the global variables at the top we defined some
    ### metrics which will be used to validate the network
    ### along the way. this is the log file where those
    ### metric values will be saved.
    #########################################################
    callbacks = [
        tf.keras.callbacks.TensorBoard(outf+'logs',
            histogram_freq=1,
            update_freq='epoch',
            profile_batch=(1,10)),
        tf.keras.callbacks.ModelCheckpoint(
            filepath=outf+'checkpoint-{epoch:04d}')
        ]
    
    #########################################################
    ##################### training ##########################
    ### now finally we load the training data each training
    ### epoch and train the network
    #########################################################
    input_data_dir = datf + '/images_labels'
    data_paths = sorted(
        [
            os.path.join(input_data_dir, fname)
            for fname in os.listdir(input_data_dir)
            if fname.endswith(".npz")
            ])
    input_params_dir = datf + '/tem_params'
    params_paths = sorted(
        [
            os.path.join(input_params_dir, fname)
            for fname in os.listdir(input_params_dir)
            ])
    print('training data:', len(data_paths), ' samples')
    train_gen = datagenerator(
            btch, imgdim, data_paths, params_paths, chan_in, chan_out,
            par['normalizedistance']
    )
    
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    input_data_dir = vdatf + '/images_labels'
    data_paths = sorted(
        [
            os.path.join(input_data_dir, fname)
            for fname in os.listdir(input_data_dir)
            if fname.endswith(".npz")
            ])
    input_params_dir = vdatf + '/tem_params'
    params_paths = sorted(
        [
            os.path.join(input_params_dir, fname)
            for fname in os.listdir(input_params_dir)
            ])
    print('validation data:', len(data_paths), ' samples')
    val_gen = datagenerator(
            btch, imgdim, data_paths, params_paths, chan_in, chan_out,
            par['normalizedistance'],augment=False
    )
    
    epochs=args.epochs
    print('tensorflow v.{}'.format(tf.__version__))
    print("starting timing")
    before = time.time()
    model.fit(
            x=train_gen,
            batch_size=btch,
            epochs=epochs,
            validation_data=val_gen,
            callbacks=callbacks,
            initial_epoch=initial_epoch
            )
    totaltime = time.time() - before
    print("time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
    
    keras_file = outf + 'model-0'
    model.save(keras_file)
    #tf.keras.models.save_model(model, keras_file, include_optimizer=False)
    print('saved model to:', keras_file)
    score = model.evaluate(
            val_gen,
            verbose=1,
            callbacks=callbacks)
    print(f'test loss: {score[0]} / test accuracy: {score[1]} / test precision: {score[2]} / test recall: {score[3]}')
    print('see tensorboard output with: $ tensorboard --logdir', outf+'logs')
